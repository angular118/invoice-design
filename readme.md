

## in package.json
 // "watch:start": "parcel serve src/index.html",
 "watch:start": "lite-server -w",
 
      "build": "npm-run-all --parallel 'prod:*'",
      



      <footer>
        <div class="container-lg p-7">
          <div class="grid grid-rows-3">
            <!-- first  -->
            <div class="text-left">
              <img class="h-3" alt="PumPumPum Logo" src="./logo.svg" />
            </div>
            <!-- second -->
            <div class="grid grid-cols-4 mt-5 text-xs">
              <div>
                <p class="">
                  PumPumPum is a flexible, affordable and convenient solution
                  for car ownership.
                </p>
                <button class="px-3 py-1.5 rounded text-white bg-red-600">
                  Corporate Lease
                </button>
              </div>
              <div class="third">
                <p>Contact Us</p>
                <p>Explore Cars</p>
              </div>
              <!--  -->
              <div class="footer-description footer-side">
                <p>
                  <a href="/faqs">FAQs</a>
                </p>
                <p>
                  <a href="/terms-condition">Terms &amp; Conditions</a>
                </p>
                <p>
                  <a href="/privacy-policy">Privacy Policy</a>
                </p>
                <p>
                  <a href="/refund">Refund Policy</a>
                </p>
              </div>

              <div class="">
                <div class="footer-description footer-side contact-block">
                  <p>
                    <a href="#"
                      ><span class="icon-phone"></span> +91 8178050450</a
                    >
                  </p>
                  <p>
                    <a href="mailto:support@pumpumpum.com"
                      ><span class="icon-email"></span>
                      support@pumpumpum.com
                    </a>
                  </p>
                  <div class="social-media">
                    <p>
                      <a href="https://www.facebook.com/pumpumpumcars/">
                        PumPumPum
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <!-- third  -->
            <div class="grid grid-cols-2">
              <div class="col-span-6">
                <p>© 2021 Pumpumpum.com</p>
              </div>
              <div class="col-span-6">T9L image</div>
            </div>
          </div>
          <!--  -->
        </div>

        <div class="footer-background">.</div>
      </footer> 