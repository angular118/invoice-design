module.exports = {
    mode: 'jit',
    purge: ['./src/*.html',
        './src/*.js',],
    darkMode: false, // or 'media' or 'class'
    theme: {

        container: {
            padding: '2rem',
        },
        extend: {
            boxShadow: {
                'box': '0 0 40px 0 rgba(0, 0, 0, 0.07)',
            },
            flex: {
                '3': '3',
                '1/2': '0 0 50%',
            },
            maxWidth: {
                '50%': '50%',
            },
            minWidth: {
                '50%': '50%',
            },
            padding: {
                '15': '3.75rem'
            },
            borderColor: theme => ({
                ...theme('colors'),
                // DEFAULT: theme('colors.gray.300', 'currentColor'),
                'button-subscribe': '#ff9bb4',

            }),
            backgroundColor: theme => ({
                'redColor': '#ee1f52',
            }),
            backgroundImage: {
                'banner': "url('./banner-01.png')",
                'logo': "url('./logo.svg')",
                'bezier': "url('./bg.svg')",
                'yellow-dots': "url('./yellow-dots-1.png')",
                'questionmark': "url('./questionmark.svg')",
                'pink-dots': "url('./pink-dots-1.png')",
                'pink-dots-2': "url('./pink-dots-2.png')",
                'grey-dots': "url('./pink-dots-1.png')",
                "quote-grey": "url('./quotes-grey.svg')"

            },
            backgroundSize: {
                "20%":"20%",
                '50%': '50%',
                '18': '10rem',
            },
            backgroundPosition: {
                '60-top': '60% top',
                "quote-align": ' 100px -40px',
                "pink-dots-align": " -10px  30px"
            },
            height: {
                '630': '630px'
            },
            textColor: {
                'redColor': '#ee1f52',
            }
        },
    },
    variants: {},
    plugins: [
        // require('daisyui'),
    ],
    // daisyui: {
    //     styled: false,
    // },

}
